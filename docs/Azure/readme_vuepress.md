# Azure Information for AZ-102 Microsoft Administrator Certification Transition:




### Exam Prep:

- <input type="checkbox">  [Azure Documention](https://docs.microsoft.com/en-us/azure/)
    - <input type="checkbox">  [Free Azure Courses from Pluralsight](https://www.pluralsight.com/partners/microsoft/azure?aid=7010a000001xDURAA2) Microsoft Azure Administrator, Microsoft Azure Developer, Microsoft Azure Solution Architect.
- <input type="checkbox">  [Microsoft Learn](https://docs.microsoft.com/en-us/learn/) Select the Path / Role


- <input type="checkbox"> Udemy (Scott Duffy) - [AZ-102 Azure Administrator Transition Exam Cert Prep](https://www.udemy.com/az102-azure/)

### Pratice Exams:

- <input type="checkbox"> Quizlet - [New AZ-102 Azure Administrator Transition exam questions](https://quizlet.com/355787928/new-az-102-azure-administrator-transition-exam-questions-flash-cards/)


### Resources:
- <input type="checkbox" checked> Pixel Robots - [Study resources for the AZ-102 Microsoft Certified Azure Administrator](https://pixelrobots.co.uk/2018/07/study-resources-for-the-az-102-microsoft-certified-azure-administrator/)


### Exam Reviews / Beta:
- Sharon Bennett - Linkedin [Microsoft Exam AZ-102 Beta: A Fun Exam](https://www.linkedin.com/pulse/microsoft-exam-az-102-beta-fun-sharon-bennett/)
- Charbel Nemnom  [Passed Exam AZ-102: Microsoft Azure Administrator Certification #Microsoft #Azure @MSLearning](https://charbelnemnom.com/2018/10/passed-exam-az-102-microsoft-azure-administrator-certification-microsoft-azure-mslearning/)
- Sarah Lean - TechieLass [Passing AZ-102](https://techielass.com/passing-az-102)
    - Scott Duffy [Software Architect.ca](https://softwarearchitect.ca/)

### Test Centre:
Tr@ining26<br>
Leeds Media Centre<br>
21 Savile Mount<br>
Leeds<br>
West Yorkshire<br>
LS7 3HZ<br><br><br>


| Author | Website | Comment
|:-|:-|:-
| Sarah Lean | [TechieLass](https://techielass.com/) | Usefull Resources for Azure etc...
| w3Schools | [HTML "<"input">" checked Attribute](https://www.w3schools.com/tags/att_input_checked.asp) | Unable to get Markdown Checkbox to work




