# Azure Information for AZ-102 Microsoft Administrator Certification Transition:



### Exam Outline:
* **Microsoft** [Exam AZ-102: Microsoft Azure Administrator Certification Transition](https://www.microsoft.com/en-us/learning/exam-az-102.aspx)
* Microsoft Azure certification exams [Exam AZ-102](https://www.microsoft.com/en-us/learning/azure-exams.aspx#exam-az-102-section)

### Exam Prep:
* [ ] [Azure Documention](https://docs.microsoft.com/en-us/azure/)
    * [ ] [Free Azure Courses from **Pluralsight**](https://www.pluralsighcom/partners/microsoft/azure?aid=7010a000001xDURAA2) Microsoft Azure Administrator, Microsoft Azure Developer, Microsoft Azure Solution Architect.
* [ ] Microsoft Learn > [Browse All](https://docs.microsoft.com/en-us/learn/browse/?products=azure&roles=administrator&resource_type=learning%20path)
* [ ] [Microsoft Learn](https://docs.microsoft.com/en-us/learn/) Select the Path / Role
* [ ] Skylines Academy - AZ-100 Microsoft Azure Infrastructure And Deployment
* [ ] Skylines Academy - AZ-101 Microsoft Azure Integration and Security
* [ ] Skylines Academy - [Microsoft AZ-102 - Azure Administrator Cert Transition](https://www.udemy.com/az-102-skylines-academy/)
* [ ] Skylines Academy - [Microsoft AZ-103: Azure Administrator Exam Certification](https://www.udemy.com/course/az-100-skylines-academy/)
* [x] Udemy (Scott Duffy) - [AZ-102 Azure Administrator Transition Exam Cert Prep](https://www.udemy.com/az102-azure/)
    * [Micorsoft Hands-Ob Labs - Self Paced](https://www.microsoft.com/handsonlabs/selfpacedlabs)
        * [ ] Simply search for "Deep Analysis with Microsoft Azure Log Analytics" to launch it.
        * [ ] Simply search for "Azure Networking Concepts" to launch it.
        * [ ] Simply search for "Identity Services with Active Directory" to launch it.
        * [ ] [For some extra practice, check out the hands on labs about Azure Migrate:](https://handsonlabs.microsoft.com/handsonlabs/Account/SignIn?redirect=/handsonlabs/SelfPacedLabs?storyId=story://content-private/content/w-migrate/1-azuremigrate/a_migrate)
        * [ ] Simply search for "Azure Site Recovery" to launch it.
        * [ ] For some extra practice, log into the Azure Portal and create a Function App. It's free.
            <br>**Assignment:**
            <br>Create a new function app, triggered by a HTTP request
            <br>Accept two numbers as input on the querystring
            <br>Add them together and return the result to the caller by HTTP response
        * [ ] Simply search for "Azure Networking Concepts" to launch it.
        * [ ] There is also another called "Understanding Azure Networking".
        * [ ] And also "Deep Analysis with Microsoft Azure Log Analytics".
        * [ ] If you do not have an Azure Active Directory on your account, create one at the free tier.
<br>Investigate how users and groups work, as well as roles, using your own Azure AD.
<br>If you are willing to do the "free trial" of the Premium tier, do that.
<br>Then enable MFA and conditional access for admin users.
* [ ] CBT Nuggets - Cloud Concepts AZ-900
* [ ] CBT Nuggets - Core Azure Services AZ-900
* [ ] CBT Nuggets - Advanced Virtual Networking With Azure AZ-101
* [ ] CBT Nuggets - Azure App Services AZ-101
* [ ] Lynda - Exam Prep - Microsoft Azure Infrastructure and Deployment (AZ-100)
* [x] Lynda - Cert Prep - Microsoft Azure Administrator Certification Transition Exam AZ-102
    * [Implement and Manage Virtual Networking](https://www.lynda.com/Azure-tutorials/Implement-manage-virtual-networking/5005068/5023233-4.html)
<br>###### Exam Prep: Microsoft Azure Infrastructure and Deployment (AZ-100) ######
    * [ ] [Microsoft Azure: Backup and Disaster Recovery](https://www.lynda.com/Azure-tutorials/Microsoft-Azure-Disaster-Recovery/685032-2.html)
    * [ ] [Azure Serverless Computing](https://www.lynda.com/Azure-tutorials/Azure-Serverless-Computing/711816-2.html)
    * [ ] [Microsoft Azure: Implement Azure Active Directory](https://www.lynda.com/Azure-tutorials/Microsoft-Azure-Implement-Azure-Active-Directory/534640-2.html)
<br>--
    * [ ] [Serverless Architecture](https://www.lynda.com/Developer-tutorials/Serverless-Architectures/774902-2.html)
    * [ ] [Learning Cloud Computing: Serverless Computing](https://www.lynda.com/course-tutorials/Learning-Cloud-Computing-Serverless-Computing/728399-2.html)
    * [Lynda.com - Cloud Computing Training Tutorials](https://www.lynda.com/Cloud-Computing-training-tutorials/1385-0.html)
* [ ] A Cloud Guru *(Not Purchased)* - [AZ-102 Microsoft Azure Administrator Certification Transition](https://acloud.guru/learn/az-102-administration-transition)
<br>**Non Exam Specific**
    * [ ] Intellezy - Azure Introduction To Azure

### Pratice Exams:
* [ ] Udemy (Scot Duffy & Riann Lowe) [AZ-100/103 Azure Administrator Infrastructure **2x** Practice Test](https://www.udemy.com/az-100-azure-administrator-infrastructure-practice-test/)
* [ ] WhizLabs - Free Pratice Test [Microsoft Azure Exam AZ-102 Certification](https://www.whizlabs.com/microsoft-azure-certification-az-102/free-test/)
* [ ] Quizlet - [New AZ-102 Azure Administrator Transition exam questions](https://quizlet.com/355787928/new-az-102-azure-administrator-transition-exam-questions-flash-cards/)

### Resources:
* [ ] Pixel Robots - [Study resources for the AZ-102 Microsoft Certified Azure Administrator](https://pixelrobots.co.uk/2018/07/study-resources-for-the-az-102-microsoft-certified-azure-administrator/)
* [ ] Stanislas.io (Stanislas Quastana) - [Preparation Guide for Microsoft AZ-102 Microsoft Azure Administrator Certification Transition](https://stanislas.io/2018/10/09/preparation-guide-for-microsoft-az-102-microsoft-azure-administrator-certification-transition/)
* [ ] WhizLabs (Neeru Jain) - [How to Prepare for Microsoft Azure Exam AZ-102?](https://www.whizlabs.com/blog/az-102-exam-preparation/)


### Exam Reviews / Beta:
- Sharon Bennett - Linkedin [Microsoft Exam AZ-102 Beta: A Fun Exam](https://www.linkedin.com/pulse/microsoft-exam-az-102-beta-fun-sharon-bennett/)
- Charbel Nemnom  [Passed Exam AZ-102: Microsoft Azure Administrator Certification #Microsoft #Azure @MSLearning](https://charbelnemnom.com/2018/10/passed-exam-az-102-microsoft-azure-administrator-certification-microsoft-azure-mslearning/)
- Sarah Lean - TechieLass [Passing AZ-102](https://techielass.com/passing-az-102)
    - Scott Duffy [Software Architect.ca](https://softwarearchitect.ca/)
- Doug Vanderweide [Microsoft Azure Exam AZ-102 Beta: First Impressions](https://www.dougv.com/2018/07/microsoft-azure-exam-az-102-beta-first-impressions/)

### Test Centre:
Tr@ining26<br>
Leeds Media Centre<br>
21 Savile Mount<br>
Leeds<br>
West Yorkshire<br>
LS7 3HZ<br><br><br>


| Author | Website | Comment
|:-|:-|:-
| Sarah Lean | [TechieLass](https://techielass.com/) | Usefull Resources for Azure etc...
| w3Schools | [HTML "<"input">" checked Attribute](https://www.w3schools.com/tags/att_input_checked.asp) | Unable to get Markdown Checkbox to work



### Future Resources:

| Author | Website | Comment
|:-|:-|:-
| BuildAzure | [AZ-103 Microsoft Azure Administrator Certification Exam](https://buildazure.com/2019/03/21/az-103-microsoft-azure-administrator-certification/) |
| Sarah Lean | [Pricing up your Azure Environment](https://techielass.com/pricing-up-your-environment) | Reviews VMChooser, AZPrice,
| Karim Vaies | [http://kvaes.be/](http://kvaes.be/) | His Personal site, creator of vmchooser.com, works for MS
| VMChooser | [www.vmchooser.com](https://www.vmchooser.com/) |
| Azure Pirce Calculator | [azprice.info](https://azprice.info/) |
| Microsoft | [Microsoft Azure certification exams](https://www.microsoft.com/en-us/learning/azure-exams.aspx) |
| Udemy | Scott Duffy - [AZ-300 Azure Architecture Technologies Certification Exam](https://www.udemy.com/70534-azure/) |
| Udemy | Scott Duffy & Riann Lowe - [AZ-300 Azure Architecture Technologies Practice Test](https://www.udemy.com/course/az-300-azure-architecture-technologies-practice-test/)
| Microsoft | [Exam AZ-103: Microsoft Azure Administrator](https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RWtY7Z) | Azure Administrator Learning Path (March 2019) PDF
| Microsoft | Azure Docs - [Azure Architecture Center](https://docs.microsoft.com/en-us/azure/architecture/) |
| Microsoft | [Azure Solution Archictures](https://azure.microsoft.com/en-us/solutions/architecture/) |
| Microsoft | Downloads - [SDKs and Command Lines](https://azure.microsoft.com/en-us/downloads/?sdk=net) |
| Microsoft | [Microsoft Azure Cloud and AI Symbol / Icon Set - SVG](https://www.microsoft.com/en-us/download/details.aspx?id=41937) |
| [Nicholas Rogoff](https://blog.nicholasrogoff.com/) | [Useful Resources](https://blog.nicholasrogoff.com/resources/) |